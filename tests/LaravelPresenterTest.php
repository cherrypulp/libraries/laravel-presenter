<?php

namespace Cherrypulp\LaravelPresenter\Tests;

use Cherrypulp\LaravelPresenter\Facades\LaravelPresenter;
use Cherrypulp\LaravelPresenter\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelPresenterTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-presenter' => LaravelPresenter::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
