<?php

namespace Cherrypulp\Presenter\Contracts;


interface CardPresentableInterface
{
    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getThumb();


    /**
     * @return string
     */
    public function getLink();
}