<?php

namespace Cherrypulp\Presenter\Contracts;


interface SeoPresentableInterface
{
    /**
     * @return mixed
     */
    public function getSeoTitle();

    /**
     * @return mixed
     */
    public function getSeoDescription();

    /**
     * @return mixed
     */
    public function getSeoKeywords();

    /**
     * @return mixed
     */
    public function getSeoThumb();
}