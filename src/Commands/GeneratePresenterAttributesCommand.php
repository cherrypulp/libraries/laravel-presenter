<?php

namespace Cherrypulp\Presenter\Commands;

use Illuminate\Console\Command;
use ReflectionClass;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GeneratePresenterAttributesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'presenter:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate attributes into presenter';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Presenter';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            [
                'name', InputArgument::REQUIRED, 'The name of the '.$this->type
            ],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'The base model of this '.$this->type.' if null will guess it.'],
            ['fields', 'f', InputOption::VALUE_OPTIONAL, 'The fields to add to the '.$this->type.'.'],
        ];
    }

    public function handle(){


        $class = $this->argument("name");

        $filePath = base_path(config("laravel-presenter.path")."/".$class.".php");

        $namespacedClass = config("laravel-presenter.namespace")."\\".$class;

        $reflectionClass = new ReflectionClass($namespacedClass);



        if ($fields = $this->option("fields")) {
            $structure = explode(',', $fields);
        } elseif ($model = $this->option("model")) {

            $modelClass = new ReflectionClass($model);

            if ($modelClass->hasProperty("presentable")) {
                $structure = $modelClass->getProperty("presentable");
            } else {
                $structure = $modelClass->getProperty("fillable");
            }

        } elseif($reflectionClass->hasProperty("attributes")){
            $structure = $reflectionClass->getProperty("attributes")->getValue();
        } else {
            $this->error("You must enter a fields option or at least a model option");
        }

        $data = file_get_contents($filePath);

        $start = strpos($data, "//@start-structure\n");
        $end = strpos($data, "//@end-structure\n");

        if (!$start) {
            $start = strrpos($data, "}");
        }

        if (!$end) {
            $end = strrpos($data, "}");
        } else {
            $end = $end + strlen("//@end-structure");
        }

        $append = substr($data, $end);

        $data = substr($data, 0, $start);

        $data .= "//@start-structure\n";

        $formattedStructure = [];

        foreach ($structure as $key) {
            if (preg_match("/:/i", $key)) {
                list($k, $v) = explode(":", $key);
            } else {
                $k = $key;
                $v = "string";
            }

            $formattedStructure[$k] = $v;
        }

        foreach ($formattedStructure as $key => $value) {

            $key = ($key);

            $data .= "
    /**
    * @return $value
    */                
    public function get".ucfirst(camel_case($key))."(){
        return \$this->entity->$key;
    }\n";
        }
        $data .= "//@end-structure\n";
        $data .= $append;

        file_put_contents($filePath, $data);
    }
}