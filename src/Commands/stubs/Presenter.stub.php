<?php

namespace DummyNamespace;

use Cherrypulp\Presenter\Presenter;

class DummyClass extends Presenter
{
    /**
     * @var DummyModel
     */
    protected $entity;

    /**
     * @param $entity
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param $instance
     *
     * @return \DummyNamespace\DummyClass
     */
    public static function make(DummyModel $instance){
        return new self($instance);
    }
}