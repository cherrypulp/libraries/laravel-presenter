<?php

namespace Cherrypulp\Presenter;

use Cherrypulp\Presenter\Commands\GeneratePresenterAttributesCommand;
use Cherrypulp\Presenter\Commands\MakePresenterCommand;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/laravel-presenter.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('laravel-presenter.php'),
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                MakePresenterCommand::class,
                GeneratePresenterAttributesCommand::class
            ]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'laravel-presenter'
        );
    }
}
