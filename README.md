# Laravel Presenter

[![Build Status](https://travis-ci.org/cherrypulp/laravel-presenter.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-presenter)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/laravel-presenter/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/laravel-presenter/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/cherrypulp/laravel-presenter/badge.svg?branch=master)](https://coveralls.io/github/cherrypulp/laravel-presenter?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-presenter.svg)](https://packagist.org/packages/cherrypulp/laravel-presenter)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-presenter/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-presenter)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-presenter.svg)](https://packagist.org/packages/cherrypulp/laravel-presenter)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-presenter
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelPresenter\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelPresenter\Facades\LaravelPresenter::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelPresenter\ServiceProvider" --tag="config"
```

## Usage

CHANGE ME

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [](https://github.com/blok/laravel-presenter)
- [All contributors](https://github.com/cherrypulp/laravel-presenter/graphs/contributors)

This package is bootstrapped with the help of
[blok/laravel-package-generator](https://github.com/blok/laravel-package-generator).
